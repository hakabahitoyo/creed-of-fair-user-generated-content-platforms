# Creed of _fair_ user-generated content platforms

## en: Creed of _fair_ user-generated content platforms

There is a hardcore of user generated content platforms which is not too televisionized nor telephonized. _Fair_ means _is not too televisonized_. In the other words,  the users' presence is not oligopolized in a _fair_ user-generated content platform. _Open_ means _is not too telephonized_. The social graph connects various social groups in an _open_ user-generated content platform.

An _user discovery method_ is a method to meet other users in an user-generated content platform. A _fair_ user discovery method prevents the oligopoly of presence of the users, in the other words, supports the decentralization of the users.

A qualitative evaluation of the _fairness_ of an user discovery method is to check following anti-patterns: _oldies effect, positive feedback, power user effect,_ and _shrink-wrap effect_. These anti-patterns bring _unfairness_ to the user discover methods.

_Oldies effect_ is the effect that some users who had been popular in the past still are recommended ever. In a _positive feedbacked_ user discovery method, if some users had beed recommended, those users are further more recommended due to the recommendation itself. _Power user effect_ is the effect that some users who post their content so much tend to be recommended. A _shrink-wrap effected_ user discovery method shows the attributes of the users such as their avatars or names, not contents.

A quantitative evaluation of the _fairness_ of an user discovery method is the geometric mean of the number of followers (or friends) of the top N (N is a constant number) users who are recommended. This quantitative evaluation detects the oldies effect and the positive feedback, not detects the power user effect nor shrink-wrap effect. This is not a serious weakness. There are too many _unfair_ user discovery methods which are oldies effected and positive feedbacked.

## ja: フェアなユーザージェネレイテッドコンテントプラットフォームについての信条

ユーザージェネレイテッドコンテントプラットフォームには、テレビすぎず、なおかつ、電話すぎない、ハードコアがある。フェアであるとは、テレビすぎないことである。すなわち、ユーザーのプレゼンスが寡占されていないことである。オープンであるとは、電話すぎないことである。すなわち、ソーシャルグラフが多様な社会集団にまたがって成長していることである。

ユーザーディスカバリーメソッドは、ユーザージェネレイテッドコンテントプラットフォームにおいて、ユーザーが他の未知のユーザーを発見する手段である。ユーザーディスカバリーメソッドがフェアであるとは、ユーザーのプレゼンスの寡占を防ぐこと、すなわち、ユーザーの脱中央集権を助けることである。

ユーザーディスカバリーメソッドがフェアであるかどうかを定性的に評価するには、以下のアンチパターンが作用しているかどうかを調べればよい: 大御所効果、ポジティブフィードバック、パワーユーザー効果、シュリンクラップ効果。これらのアンチパターンが作用していればいるほど、そのユーザーディスカバリーメソッドはアンフェア (フェアの逆) である。

大御所効果とは、過去に有名であったユーザーが、いつまでも推挙され続けることである。ポジティブフィードバックとは、あるユーザーがユーザーディスカバリーメソッドによって推挙されると、それ自体が原因となって、そのユーザーがますます推挙されることである。パワーユーザー効果とは、頻繁に情報を発信しているユーザーが、ユーザーディスカバリーメソッドに推挙されることである。シュリンクラップエフェクトとは、ユーザーディスカバリーメソッドが、例えばアバター画像やスクリーンネームのようなユーザーの属性のみを表示し、実際のコンテンツを表示しないことである。

ユーザーディスカバリーメソッドがフェアであるかどうかを定量的に評価するには、ユーザーディスカバリーメソッドが推挙する上位N人 (Nは既定の定数) のユーザーの、フォロワー数の幾何平均を求める。なぜなら、フォロワー数が多い順に推挙するユーザーディスカバリーメソッドは、定性的な評価により、最もアンフェアであることが明らかだからである。この定量的評価は、大御所効果とポジティブフィードバックを検出するが、パワーユーザー効果とシュリンクラップ効果を検出しない。経験的には、あまりにも多くのユーザーディスカバリーメソッドが、大御所効果とポジティブフィードバックを持ち、したがって定性的にも定量的にも著しくアンフェアであることが知られている。
